var gulp=require('gulp');
var wrap=require('gulp-umd');

gulp.task('default',function(){
    gulp.src('main.js')
	.pipe(wrap({exports:function(file){return 'maximoplus'},namespace:function(file){return 'maximoplus'}}))
	.pipe(gulp.dest('dist'));
});
